package com.example.repoinherit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepoinheritApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepoinheritApplication.class, args);
    }

}
