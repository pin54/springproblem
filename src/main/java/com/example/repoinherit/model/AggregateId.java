package com.example.repoinherit.model;

import org.hibernate.annotations.Type;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;

import java.io.Serializable;
import java.util.UUID;

@Embeddable
@Access(AccessType.FIELD)
@MappedSuperclass
public class AggregateId implements Serializable {

    @Type(type = "org.hibernate.type.UUIDCharType")
    protected UUID id;

    public AggregateId(String id) {
        this.id = UUID.fromString(id);
    }

    public AggregateId(UUID id) { this.id = id; }

    protected AggregateId() {
    }
}
