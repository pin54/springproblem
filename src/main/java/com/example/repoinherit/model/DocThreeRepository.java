package com.example.repoinherit.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocThreeRepository extends JpaRepository<DocThree, DocThreeId> {


}
