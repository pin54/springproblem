package com.example.repoinherit.model;

import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Embeddable
@MappedSuperclass
public class DocOneId extends AggregateId {

    public DocOneId(String id) { super(id); }

    public DocOneId(UUID id) { super(id); }

    public DocOneId() {

    }
}
