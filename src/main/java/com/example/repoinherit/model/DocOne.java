package com.example.repoinherit.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("one")
public class DocOne extends Document<DocOneId> {

    public String docOneField;

    public DocOne(DocOneId id) {
        super(id);
        docOneField = "one";
    }

    public DocOne() {

    }
}
