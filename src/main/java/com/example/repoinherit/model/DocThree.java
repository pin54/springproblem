package com.example.repoinherit.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("three")
public class DocThree extends Document<DocThreeId> {

    public String docThreeField;

    public DocThree(DocThreeId id) {
        super(id);
        docThreeField = "three";
    }

    public DocThree() {

    }
}
