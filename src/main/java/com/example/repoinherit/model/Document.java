package com.example.repoinherit.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", columnDefinition = "varchar(10)")
@Table(name = "documents")
public abstract class Document<ID extends AggregateId> extends AggregateRoot<ID> {

    protected String number;

    public Document(ID id) {
//        this.id = id;
        super(id);
    }

    public Document() {

    }
}
