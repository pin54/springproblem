//package com.example.repoinherit.model;
//
//import com.zaxxer.hikari.HikariDataSource;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.sql.DataSource;
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(
//        basePackages = {
//                "com.example.repoinherit.model"
//        },
//        entityManagerFactoryRef = "productionEntityManagerFactory",
//        transactionManagerRef= "productionTransactionManager"
//)
//public class DatabaseConfigurationProduction {
//
//    @Bean
//    @Primary
//    @ConfigurationProperties("app.datasource.production")
//    public DataSourceProperties productionDataSourceProperties() {
//        return new DataSourceProperties();
//    }
//
//    @Bean
//    @Primary
//    @ConfigurationProperties("app.spring.jpa.production")
//    public Map<String, String> productionJpaProperties() {
//        return new HashMap<>();
//    }
//
//    @Bean
//    @Primary
//    @ConfigurationProperties("app.datasource.production.configuration")
//    public DataSource productionDataSource() {
//        return productionDataSourceProperties().initializeDataSourceBuilder()
//                .type(HikariDataSource.class).build();
//    }
//
//    @Bean(name = "productionEntityManagerFactory")
//    @Primary
//    public LocalContainerEntityManagerFactoryBean productionEntityManagerFactory(EntityManagerFactoryBuilder builder) {
//        System.out.println(productionJpaProperties());
//        return builder
//                .dataSource(productionDataSource())
//                .packages(new String[] {
//                        "com.example.repoinherit.model"
//                })
//                .properties(productionJpaProperties())
//                .build();
//    }
//
//    @Primary
//    @Bean
//    public PlatformTransactionManager productionTransactionManager(
//            final @Qualifier("productionEntityManagerFactory") LocalContainerEntityManagerFactoryBean productionEntityManagerFactory) {
//        return new JpaTransactionManager(productionEntityManagerFactory.getObject());
//    }
//
//}
