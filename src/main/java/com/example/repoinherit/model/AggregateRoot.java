package com.example.repoinherit.model;

import org.hibernate.annotations.Type;

import javax.persistence.EmbeddedId;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class AggregateRoot<ID extends AggregateId> {

    @EmbeddedId
    @Type(type = "org.hibernate.type.UUIDCharType")
    protected ID id;

    public AggregateRoot(ID id) {
        this.id = id;
    }

    public AggregateRoot() {

    }
}
