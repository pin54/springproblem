package com.example.repoinherit.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("two")
public class DocTwo extends Document<DocTwoId> {

    public String docTwoField;

    public DocTwo(DocTwoId id) {
        super(id);
        docTwoField = "two";
    }


    public DocTwo() {

    }
}
