package com.example.repoinherit.model;

import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Embeddable
@MappedSuperclass
public class DocThreeId extends AggregateId {

    public DocThreeId(String id) { super(id); }

    public DocThreeId(UUID id) { super(id); }

    public DocThreeId() {

    }
}
