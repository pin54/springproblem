package com.example.repoinherit.model;

import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Embeddable
@MappedSuperclass
public class DocTwoId extends AggregateId {

    public DocTwoId(String id) { super(id); }

    public DocTwoId(UUID id) { super(id); }

    public DocTwoId() {

    }
}
