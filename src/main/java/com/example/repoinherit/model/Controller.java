package com.example.repoinherit.model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/api")
@Transactional
public class Controller {

    private final DocOneRepository oneRepo;
    private final DocTwoRepository twoRepo;
    private final DocThreeRepository threeRepo;

    public Controller(DocOneRepository oneRepo,
                      DocTwoRepository twoRepo,
                      DocThreeRepository threeRepo) {
        this.oneRepo = oneRepo;
        this.threeRepo = threeRepo;
        this.twoRepo = twoRepo;
    }

    @GetMapping("/saveandfetch")
    String allDocs() {

        DocOneId oneId = new DocOneId("bb8b6c06-7ee3-4cd8-8e88-a1f18d35d0d5");
        DocTwoId twoId = new DocTwoId("9a72629d-7588-432c-aaba-d362bf756572");
        DocThreeId threeId = new DocThreeId("9a4b1e14-81b3-4af2-b908-d40cfc6d76c7");

        DocOne docOne = new DocOne(oneId);
        DocTwo docTwo = new DocTwo(twoId);
        DocThree docThree = new DocThree(threeId);

        oneRepo.save(docOne);
        twoRepo.save(docTwo);
        threeRepo.save(docThree);

        DocOne sOne = oneRepo.getById(oneId);
        DocTwo sTwo = twoRepo.getById(twoId);
        DocThree sThree = threeRepo.getById(threeId);

        return "ID: " + sOne.id.id + " " + sTwo.id.id + " " + sThree.id.id;
    }

    @GetMapping("/fetch")
    String hmm() {

        DocOneId oneId = new DocOneId("bb8b6c06-7ee3-4cd8-8e88-a1f18d35d0d5");
        DocTwoId twoId = new DocTwoId("9a72629d-7588-432c-aaba-d362bf756572");
        DocThreeId threeId = new DocThreeId("9a4b1e14-81b3-4af2-b908-d40cfc6d76c7");

        DocOne sOne = oneRepo.getById(oneId);
        DocTwo sTwo = twoRepo.getById(twoId);
        DocThree sThree = threeRepo.getById(threeId);

        return "ID: " + sOne.id.id + " " + sTwo.id.id + " " + sThree.id.id;
    }
}
